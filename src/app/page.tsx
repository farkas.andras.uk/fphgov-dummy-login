'use client';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import { useState, FormEvent } from 'react';

const errorInitState = { message: '' };

export default function App() {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<{ message: string }>(errorInitState);
  const [email, setEmail] = useState<string | null>();
  const [password, setPassword] = useState<string>('');
  const [passwordTextVisibility, setpasswordTextVisibility] =
    useState<boolean>(false);

  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setIsLoading(true);
    setError(errorInitState);

    try {
      const response = await fetch('https://dummyjson.com/auth/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          username: email,
          password: password,
        }),
      });

      const data = await response.json();

      if (!response.ok) {
        setError(data);
      } else {
        router.push('/home');
      }
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }

  return (
    <main className='flex min-h-screen flex-col justify-between font-sans'>
      <div className='absolute flex min-h-screen min-w-full items-center justify-center'>
        <div className='w-full lg:max-w-md'>
          <div className='flex flex-col items-center mb-6'>
            <Image
              className='flex flex-col items-center mb-2'
              src='/fphgov-logo.svg'
              alt='FPHGOV Logo'
              width={75}
              height={48}
              priority
            />
            <h1 className='text-3xl font-semibold text-center text-white text-2xl'>
              Bejelentkezés
            </h1>
          </div>
          <div className='p-6 bg-white rounded-md shadow-md'>
            <form className='mt-6' onSubmit={onSubmit}>
              <div className='mb-4'>
                <label
                  htmlFor='email'
                  className='block text-sm font-semibold text-primary-text text-base'
                >
                  E-mail cím:
                </label>
                <input
                  // type='email'
                  name='email'
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setEmail(e.target.value)
                  }
                  className='block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40'
                />
              </div>
              <div className='mb-4'>
                <label
                  htmlFor='password'
                  className='block text-sm font-semibold text-primary-text text-base'
                >
                  Jelszó:
                </label>
                <div className='relative'>
                  <input
                    id='hs-toggle-password'
                    name='password'
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      setPassword(e.target.value)
                    }
                    type={passwordTextVisibility ? 'text' : 'password'}
                    className='block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40'
                  />
                  <button
                    type='button'
                    className='absolute top-0 end-0 p-2.5 rounded-e-md dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600'
                    onClick={() =>
                      setpasswordTextVisibility(!passwordTextVisibility)
                    }
                  >
                    {passwordTextVisibility ? (
                      <Image
                        src='/eye_visible.svg'
                        alt='eye_visible'
                        width={20}
                        height={20}
                        priority
                      />
                    ) : (
                      <Image
                        src='/eye_hidden.svg'
                        alt='eye_hidden'
                        width={20}
                        height={20}
                        priority
                      />
                    )}
                  </button>
                </div>
              </div>

              <div className='flex items-center mb-4'>
                <input
                  id='default-checkbox'
                  type='checkbox'
                  value=''
                  className='w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600'
                />
                <label className='ms-2 text-sm font-medium text-gray-900 dark:text-gray-300'>
                  Emlékezzen rám
                </label>
              </div>
              {error && <p className='text-red-500'>{`${error.message}`}</p>}
              {error && error.message === 'Invalid credentials' && (
                <p className='text-red-500'>
                  {
                    'Csak dummyjson.com/users felhasználóknak, pl user: kminchelle pass: 0lelplR'
                  }
                </p>
              )}
              <div className='mt-2'>
                <button
                  type='submit'
                  className='w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-primary-button rounded-md'
                  disabled={isLoading}
                >
                  {isLoading ? 'Bejelentkezés folyamatban...' : 'Bejelentkezés'}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div className='flex min-h-screen flex-col items-end'>
        <div
          className='absolute flex min-h-screen items-end -z-50'
          style={{ width: '472px', height: '622px' }}
        >
          <Image
            src='/fphgov-bg-logo.svg'
            alt='FPHGOV Logo'
            width={0}
            height={0}
            style={{ width: '100%', height: '100%' }}
            priority
          />
        </div>
      </div>
    </main>
  );
}
