'use client';
import { useRouter } from 'next/navigation';

export default function Home() {
  const router = useRouter();

  const redirectToLogin = () => {
    router.push('/');
  };

  return (
    <main className='flex min-h-screen flex-col justify-between font-sans'>
      <div className='flex min-h-screen items-center justify-center'>
        <button
          type='button'
          className='px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-primary-button rounded-md'
          onClick={() => redirectToLogin()}
        >
          {'Kijelentkezés'}
        </button>
      </div>
    </main>
  );
}
